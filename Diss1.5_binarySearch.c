/** 算法：二分法，binary search
 * 时间复杂度和空间复杂度分析
 * 查找算法中的“二分法”是这样定义的：

    给定N个从小到大排好序的整数序列List[]，以及某待查找整数X，
    我们的目标是找到X在List中的下标。
    即若有List[i]=X，则返回i；否则返回-1表示没有找到。
    二分法是先找到序列的中点List[M]，
    与X进行比较，若相等则返回中点下标；
    否则，若List[M]>X，则在左边的子系列中查找X；若List[M]<X，
    则在右边的子系列中查找X。
    试写出算法的伪码描述，并分析最坏、最好情况下的时间、空间复杂度。
 * 
 */
#include <stdio.h>

int BinarySearch(int SortedList[], int n, int check)
{
    int ret = -1;
    int start = 0;
    int end = n - 1;
    int mid = (start + end)/2;
    while (start < end)
    {
        if (SortedList[mid] == check)
        {
            ret = mid;
            break;
        }
        else if (SortedList[mid] < check)
        {
            start = mid + 1;
        }
        else
        {
            end = mid - 1;
        }
        mid = (end + start)/2;
    }
    return ret;
}