/*
 * @Author: duqinghao
 * @Date: 2020-04-29 08:39:39
 * @LastEditTime: 2020-05-07 09:23:15
 * @LastEditors: Please set LastEditors
 * @Description: 实现简单的Shell
 * @FilePath: \AlgorithmLearn\simpleShell.c
 */
/**
 * @description: 练习：实现简单的Shell
用讲过的各种C函数实现一个简单的交互式Shell，要求：
1、给出提示符，让用户输入一行命令，识别程序名和参数并调用适当的exec函数执行程序，待
执行完成后再次给出提示符。
2、识别和处理以下符号：
简单的标准输入输出重定向（<和>）：仿照例 30.5 “wrapper”，先dup2然后exec。
管道（|）：Shell进程先调用pipe创建一对管道描述符，然后fork出两个子进程，一个子
进程关闭读端，调用dup2把写端赋给标准输出，另一个子进程关闭写端，调用dup2把读端
赋给标准输入，两个子进程分别调用exec执行程序，而Shell进程把管道的两端都关闭，调
用wait等待两个子进程终止。
你的程序应该可以处理以下命令：
○ls△-l△-R○>○file1○
○cat○<○file1○|○wc△-c○>○file1○
○表示零个或多个空格，△表示一个或多个空格
 * @param {type} 
 * @return: 
 */

#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#define MAX_STR_NUM  100

int split_user_input(char input[], char *para[])
{
    char *pstart = &input[0];
    char *pend = pstart;
    int commond_num = 0;
    while (*pend != '\n' && *pstart != '\0')
    {
        if(*pstart != ' ')
        {
            pend++;
        }
        else
        {
            pstart++;
            pend++;
        }
        if (*pend == ' ')
        {
            para[commond_num++] = pstart;
            *pend = '\0';
            pend++;
            pstart = pend;
        }
        
    }
    if (pend != pstart)
    {
        para[commond_num++] = pstart;
        *pend = '\0';
    }
    
    return commond_num;
}

int main(void)
{
    while(1)
    {
        char *pexe_commond[10] = {NULL};
        char *pexe_para[10] = {NULL};
        char userinput[MAX_STR_NUM] = {0};
        int i = 0, num_cmd = 0;
        
        printf("input here$: ");
        
        read(STDIN_FILENO, &userinput, MAX_STR_NUM);

        num_cmd = split_user_input(userinput, pexe_commond);
        /* fork 一个新的进程，并且执行解析出的命令 */
        pid_t pid = 0;//fork();
        if (pid < 0)
        {
            perror("fork");
        }
        else if (pid == 0)
        {
            /* child */
        }
        else
        {
            /* parent */
            
        }
        
        
    }
}