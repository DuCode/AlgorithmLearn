/**
 * du
 * 多项式求和计算
 * */
#include <stdio.h>
#include <math.h>
#include <time.h>
#define MAX_RUN_TIME 10000000
typedef double (*pFun)(int n, double x);
double f1(int n, double x)
{
    double ret = 1.0;
    int i = 1;
    for (i; i <= n; i++)
    {
        ret += 1/i * pow(x,i);
    }
    return ret;
}
/* 采用更高效的方式 */
double f2(int n,  double x)
{
    double dret = 1/n;
    int i;
    for (i = n; i > 0; i--)
    {
        dret = x*dret + 1/(i-1);
    }
    return dret;
}

double runTime(pFun funcall)
{
    clock_t startTime = 0, endTime = 0;
    double result = 0.0, duration = 0.0;
    int i = 0;
    startTime = clock();
    for (i = 0; i < MAX_RUN_TIME; i++)
    {
        result = f1(100, 1.1);
    }
    endTime = clock();
    duration = (endTime - startTime);
}

void Test_Func(void)
{
    printf("%.3e\n", runTime(f1)) ;
    printf("%.3e\n", runTime(f2)) ;
}