#include <stdio.h>
#include <string.h>
#include "Stack.h"


pStack stack_New(void)
{
    pStack NewStack = (pStack)malloc(sizeof(Stack_t));
    memset(NewStack->data, 0, sizeof(NewStack->data));
    NewStack->top = -1;
    return NewStack;
}

int stack_isEmpty(Stack_t *s)
{
    if (s)
    {
        if (s->top == -1)
            return 1;
        else
            return 0;
    }
    else
        return 1;
    
}

ElementType pop(pStack s)
{
    if (!stack_isEmpty(s))
    {
        return s->data[(s->top)--];
    }
}

void push(pStack s, ElementType x)
{
    if (s && s->top == MAX_SIZE - 1)
    {
        s->data[++(s->top)];
    }
}

ElementType stack_gettop(pStack s)
{
    return s->data[s->top];
}


/* 用一个数组实现两个堆栈： 要求数组有空间时，堆栈就能如入栈 */
/**  实现方式： 两个堆栈分别用数组的两端作为起点，堆栈的生长方向向数组中间前进，当堆栈1的top1 + 1 == top2时，堆栈1为下标较小的一个  **/
#if 0
DStack_t DStack;

DStack.top1 = -1;
Dstack.top2 = MAX_SIZE;

int push(ElementType x, DStack_t *s, int tag)
{
    if ((s->top1 + 1) != s->top2)
        {
            if (tag == 0)
            {
                s->StackBuf[++(s->top1)];
            }
            else
            {
                s->StackBuf[--(s->top2)];
            }
        }
    
}

int pop(DStack_t *s, int tag)
{
    if (tag == 0 && s->top1 != -1)
    {
        s->StackBuf[(s->top1)--];
    }
    
    if (tag && s->top2 != MAX_SIZE)
    {
        s->StackBuf[(s->top2)++];
    }
    
}

/*   用宏定义实现的基于数组的堆栈实现 */
int stack[1000] = {0};
int top = -1;
#define pop()   if (top > -1) stack[top--]
#define push(x)   if (top == 1000-1) stack[++top] = (x)
#define isempty()  top == -1 ? 1 : 0
#define gettop()   if (!isempty()) stack[top]

#endif