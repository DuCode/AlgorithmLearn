#include <stdio.h>
#include <string.h>

#define NULL -1
#define MAX_SIZE 100001
typedef struct T_node * list;
typedef struct T_node
{
    int addr;
    int data;
    int next; 
    int valid;
}node_t;
int nodenum = 0;

int nextaddr[MAX_SIZE];
node_t nodeBuf[MAX_SIZE] = {0};
//简易堆栈
int top = -1;
list stack[MAX_SIZE];



#define pop(top)   ((top > -1) ? stack[top--] : 0)
#define push(x, top)   (top != MAX_SIZE-1) ? stack[++top] = (x) : 0
#define isempty(top)  ((top == -1) ? 1 : 0)
#define gettop(top)   (isempty(top) != 1 ? stack[top] : 0)


#define getnext(next) (next!=NULL ? &nodeBuf[(next)] :(list) 0)

list reverse_link(list L, int k, int revercnt)
{   
    list ret = 0;
    list Rear = 0;
    int j = 0;
    for (j = 0; j < revercnt; j++)
    {
        int i = 0;
        /* 倒序的内容入栈 */
        while (L != 0 && i < k)
        {
            push(L, top);
            L = getnext(L->next);
            i++;
        }
        if (!isempty(top))
        {
            if (ret == 0)
                ret = gettop(top);
            else
                Rear->next = gettop(top)->addr;
        }
        

        while (!isempty(top))
        {
            list n = pop(top);
            if (isempty(top))
            {
                if (L)
                    n->next = L->addr;
                else
                    n->next = NULL;
                Rear = n;
            }
            else
            {
                list topnode = gettop(top);
                n->next = topnode->addr;
            }
        
        }
    }
    return ret;
}

void printL(list L)
{
    while (L)
    {
        if (L->next == NULL)
        {
            printf("%05d %d %d", L->addr, L->data, L->next);
            break;
        }
        else
            printf("%05d %d %05d\n", L->addr, L->data, L->next);
        L = &nodeBuf[L->next];
    }
}
/* just test  */
/* TODO One-  */

int main_test(void)
{
    
    int addr, num, k, i = 0;
    list first = 0;
    scanf("%d %d %d",&addr, &num, &k);
    while (i < num)
    {
        node_t n = {0};
        n.next = NULL;
        scanf("%d %d %d", &n.addr, &n.data, &n.next);
        n.valid = 1;
        nodeBuf[n.addr] = n;
        if (n.next != NULL)
            nextaddr[n.next] = 1;
        else
        {
            nextaddr[n.addr] = 1;
            first = &nodeBuf[n.addr];
        } 
        i++;
    }
    i = 0;
    while (i < MAX_SIZE)
    {
        if (nodeBuf[i].valid && nextaddr[nodeBuf[i].addr] == 0)
        {
            list temp = &nodeBuf[i];
            first = &nodeBuf[i];
            num = 0;
            while (temp && temp->valid)
            {
                num++;
                temp = getnext(temp->next);
            }
            if (!temp)
                break;
            else
                num = 0;
        }
        i++;
    }
    list L = reverse_link(first, k, num/k);
    printL(L);
    return 0;
}



