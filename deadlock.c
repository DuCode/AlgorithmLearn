/*
 * @Author: your name
 * @Date: 2020-06-18 08:58:18
 * @LastEditTime: 2020-06-18 09:52:42
 * @LastEditors: Please set LastEditors
 * @Description: 测试哲学家吃饭造成的失锁问题
 * @FilePath: \AlgorithmLearn\deadlock.c
 */ 

 #include <stdio.h>
 #include <pthread.h>
 #include <stdlib.h>

 int resource = 10;
 pthread_mutex_t cp1,cp2,cp3,cp4,cp5;
 pthread_t PhA, PhB, PhC, PhD, PhE;

 void * PhAeating(void *arg)
 {
     while (1)
     {
        pthread_mutex_lock(&cp5);
        printf("A fetch chp 1\n");
        pthread_mutex_lock(&cp1);
        printf("A fetch chp 5\n");
        resource--;
        printf("A  eating !\n");
        sleep(rand()%5);
        pthread_mutex_unlock(&cp5);
        pthread_mutex_unlock(&cp1);
        sleep(rand()%5);
     }
     
 }
void * PhBeating(void *arg)
 {
     while (1)
     {
        pthread_mutex_lock(&cp2);
        printf("B fetch chp 2\n");
        pthread_mutex_lock(&cp1);
        printf("B fetch chp 1\n");
        resource--;
        printf("B  eating !\n");
        sleep(rand()%5);
        pthread_mutex_unlock(&cp2);
        pthread_mutex_unlock(&cp1);
        sleep(rand()%5);
     }
 }

 void * PhCeating(void *arg)
 {
     while (1)
     {
        pthread_mutex_lock(&cp3);
        printf("C fetch chp 3\n");
        pthread_mutex_lock(&cp2);
        printf("%C fetch cp 2\n");
        resource--;
        printf("C  eating !\n");
        sleep(rand()%5);
        pthread_mutex_unlock(&cp3);
        pthread_mutex_unlock(&cp2);
        sleep(rand()%5);
     }
 }

 void * PhDeating(void *arg)
 {
     while (1)
     {
        pthread_mutex_lock(&cp4);
        printf("D fetch chp 4\n");
        pthread_mutex_lock(&cp3);
        printf("D fetch chp 3\n");
        resource--;
        printf("D  eating !\n");
        sleep(rand()%5);
        pthread_mutex_unlock(&cp4);
        pthread_mutex_unlock(&cp3);
        sleep(rand()%5);
     }
 }

 void * PhEeating(void *arg)
 {
     while (1)
     {
        pthread_mutex_lock(&cp5);
        printf("E fetch chp 5\n");
        pthread_mutex_lock(&cp4);
        printf("E fetch chp 4\n");
        resource--;
        printf("E  eating !\n");
        sleep(rand()%5);
        pthread_mutex_unlock(&cp5);
        pthread_mutex_unlock(&cp4);
        sleep(rand()%5);
     }
 }

int main(void)
{
    pthread_create(&PhA, NULL, PhAeating, NULL);
    pthread_create(&PhA, NULL, PhBeating, NULL);
    pthread_create(&PhA, NULL, PhCeating, NULL);
    pthread_create(&PhA, NULL, PhDeating, NULL);
    pthread_create(&PhA, NULL, PhEeating, NULL);
    pthread_join(PhA, NULL);
    pthread_join(PhB, NULL);
    pthread_join(PhC, NULL);
    pthread_join(PhD, NULL);
    pthread_join(PhE, NULL);
    return 0u;
}
