/**  求后缀表达式  ***/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Stack.h"

/* 堆栈存储运算发符号 */
pStack stack = NULL;
char tempResult[100] = {0};
int charNum = 0;


int tokenLevel(char token)
{
    int level = 0;
    if (token == '*' || token == '/')
    {
        level = 1;
    }
    else if (token == '(')
    {
        level = -1;
    }
    
    return level;
}

int tokenCmp(char checktoken)
{
    int ret = 0;
    char toptoken = stack_gettop(stack);
    int checklevel = tokenLevel(checktoken);
    int stacklevel = tokenLevel(toptoken);
    if (checktoken == ')' && toptoken == '(')
    {
        pop(stack);
        return 0;
    }
    if (toptoken == -1)
    {
        /* 空栈直接压入操作符号 */
        push(stack, checktoken);
        return 0;
    }
    else if (checklevel <= stacklevel || checktoken == ')')
    {
        /* 弹出堆栈内的运算符 */
        tempResult[charNum++] = pop(stack);
        tokenCmp(checktoken);
    }
    else
    {
        push(stack, checktoken);
        return 0;
    }
    return ret;
}

void Calculator(void)
{
    char expressString [100] = {0};
    int ret = 0;

    stack = stack_New();
    while (scanf("%s", expressString) != EOF)
    {
        int i = 0;
        memset(tempResult, 0, sizeof(tempResult));
        charNum = 0;
        
        while (expressString[i] != '\0')
        {
            if ((expressString[i]) >= '0' && expressString[i] <= '9')
            {
                tempResult[charNum] = expressString[i];
                charNum++;
            }
            else if (expressString[i] == '(')
            {
                push(stack, expressString[i]);
            }
            else
            {
                if (stack_isEmpty(stack))
                {
                    push(stack, expressString[i]);
                }
                else
                {
                    tokenCmp(expressString[i]);
                }
            }
            i++;
        }
        while (!stack_isEmpty(stack))
        {
            tempResult[charNum++] = pop(stack);
        }
        /* 利用堆栈计算值 */
        stack->top = -1;
        for (i = 0; i < charNum; i++)
        {
            if (tempResult[i] >= '0' && tempResult[i]<= '9')
                push(stack, tempResult[i]-'0');
            else
            {
                int a = pop(stack);
                int b = pop(stack);
                switch (tempResult[i])
                {
                    case '+':
                        ret = a + b;
                        push(stack, ret);
                        break;
                    case '-':
                        ret = b - a;
                        push(stack,ret);
                        break;
                    case '*':
                        ret = a * b;
                        push(stack,ret);
                        break;
                    case '/':
                        if (b)
                            ret = b / a;
                        push(stack,ret);
                        break;
                    default:
                        break;
                }
            }
            
        }
        printf("%d", ret);
        memset(expressString, 0, sizeof(expressString));
    }
}

