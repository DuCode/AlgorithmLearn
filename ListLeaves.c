/*
 * @Author: your name
 * @Date: 2020-05-08 18:45:18
 * @LastEditTime: 2020-05-08 21:18:04
 * @LastEditors: Please set LastEditors
 * @Description: 层次遍历树：使用队列存储访问节点的左右儿子，依次访问队列中的节点
 * @FilePath: \AlgorithmLearn\ListLeaves.c
 */
#include <stdio.h>
#define MAX_SIZE 12
#define null -1

typedef struct TreeNode
{
    int value;
    int left;
    int right;
    int attchflag;
}Tnode;
typedef Tnode * Tree;
typedef struct T_Que
{
    int head;
    int tail;
    Tnode Element[MAX_SIZE];
}Que;
Tnode T[MAX_SIZE] = {0};
Que Q = {0};


int isFull(Que *Q)
{
    if (((Q->tail + 1) % MAX_SIZE) == Q->head)
    {
        return 1;
    }
    return 0;
}

int isempty(Que *Q)
{
    if (Q->head == Q->tail)
    {
        return 1;
    }
    return 0;
}

void Q_Add(Que *Q, Tnode node)
{
    if (!isFull(Q))
    {
        Q->Element[Q->tail++] = node; 
    }
}
Tnode Q_Delete(Que *Q)
{
    if (!isempty(Q))
    {
        return Q->Element[Q->head++];
    }
}

int build_Tree(Tnode T[])
{
    int cnt = 0;
    char left, right, root = 0;
    scanf("%d", &cnt);
    for (int i = 0; i < cnt; i++)
    {
        scanf(" %c %c", &left, &right);
        T[i].value = i;
        if (left != '-')
        {
            T[i].left = left - '0';
            T[T[i].left].attchflag = 1;
        }
        else
            T[i].left = null;
        if (right != '-')
        {
            T[i].right = right - '0';
            T[T[i].right].attchflag = 1;
        }
        else
            T[i].right = null;
    }
    for (int i = 0; i < cnt; i++)
    {
        if (T[i].attchflag == 0)
        {
            root = i;
            break;
        }
    }
    return root;
}

/* TODO: 完成树的先序遍历、中序遍历和后续遍历 */

void PrintLeaveOrder(int Tree)
{
    if (Tree == null)
    {
        return ;
    }
    else
    {
        int flat = 0;
        Q_Add(&Q, T[Tree]);
        while (!isempty(&Q))
        {
            
            Tnode headnod = Q_Delete(&Q);
            
            if (headnod.left != null)
            {
                Q_Add(&Q, T[headnod.left]);
            }
            if (headnod.right != null)
            {
                Q_Add(&Q, T[headnod.right]);
            }
            if (headnod.left == null && headnod.right == null)
            {
                if (flat)
                    printf(" ");
                else
                    flat = 1;
                printf("%d", headnod.value);
            }
            
        }
        
    }
    
}

int main(void)
{
    int Root = build_Tree(T);
    PrintLeaveOrder(Root);
}