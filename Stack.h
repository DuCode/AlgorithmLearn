#ifndef STACK_H_
#define STACK_H_

#define MAX_SIZE 1000
typedef char ElementType;

typedef struct t_stack
{
    ElementType data[MAX_SIZE];
    int top;
}Stack_t;
typedef Stack_t * pStack;

typedef struct t_dstack
{
    ElementType StackBuf[MAX_SIZE];
    int top1;
    int top2;
}DStack_t;

pStack stack_New(void);
int stack_isEmpty(Stack_t *s);
ElementType pop(pStack s);
void push(pStack s, ElementType x);
ElementType stack_gettop(pStack s);

#endif

