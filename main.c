#include <stdio.h>
/**
 * 把其中所有的一个或多个连续的空白字符都压缩为一个空格
 * */
char *shrink_space(char *dest, const char *src, size_t n)
{
    /* 有一个添加空格的标识 */
    int bAddSpace = 0;
    int i = 0,dsti = 0;
    for (i; i < n; i++)
    {
        if (src[i]!=' ' && src[i]!='\n' && src[i]!='\t' && src[i]!='\r')
        {
            dest[dsti] = src[i];
            dsti++;
            bAddSpace = 1;
        }
        else if (src[i]== ' ' && bAddSpace == 1)
        {
            dest[dsti] = src[i];
            dsti++;
            bAddSpace = 0;
        }
    }
}

#include <stdio.h>

int start = 0;
int end = 0;

int MaximumSubsequenceSum(int input[], int n)
{
    int MaxSum = 0, tempSum = 0, i = 0, dis = 0;
    for (i; i < n; i++)
    {
        tempSum += input[i];
        if (tempSum > MaxSum)
        {
            MaxSum = tempSum;
            end = i;
            dis = end - start;
        }
            
        if (tempSum < 0)
        {
            tempSum = 0;
            start = i + 1;
        }
            
    }
    start = end - dis;
    return MaxSum;
}

void shrink_space_test(void)
{
    char dest[100] = {'\0'};
    char *src = "This Content hoho is ok\n\
            ok?\n\
            file system\r\
            uttered words ok ok ?\t\
            end.";
    shrink_space(dest, src, strlen(src));
    printf("%s",dest);
}

void MaximumSubsequenceSum_Test(void)
{
    int n = 0;
    int value[10000] = {0};
    while (scanf("%d", &n) != EOF)
    {
        int i = 0;
        for (i; i < n; i++)
        {
            scanf("%d", &value[i]);
        }
        printf("%d ",MaximumSubsequenceSum(value, n));
        printf("%d %d", start, end);
    }
}


