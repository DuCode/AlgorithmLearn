/*
 * @Author: your name
 * @Date: 2020-04-24 17:27:58
 * @LastEditTime: 2020-05-08 15:04:43
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \AlgorithmLearn\popseq.c
 */
#include<stdio.h>
#define MAX_SIZE 1001
int items[MAX_SIZE] = {0};
int top = -1;

int pop()
{
    if (top != -1)
        return items[top--];
    else
        return -1;
}
void push(int item)
{
    if (top < (MAX_SIZE - 1))
        items[++top] = item;
}
int empty()
{
    return top == -1 ? 1 : 0;
}
int gettop()
{
    return items[top];
}

void readLine(int nums[], int n)
{
    int j = 0;
    while (j < n)
    {
        scanf("%d", &nums[j]);
        j++;
    }
}

int checkValidation(int nums[], int m, int n)
{
    int cur = 0;
    top = -1;//clear stack
    for (int i = 1; i <= n; i++)
    {
        /* 模拟这个序列的入栈过程 */
        /* 把1-n入栈 */
        push(i);
        if (top == m)
            return 0;
        while (!empty() && nums[cur] == gettop())
        {
            /* 如果栈顶元素和结果序列的当前值一致，弹出栈顶，并且比较下一个元素 */
            pop();
            cur++;
        }
    }
    if (empty())
        return 1;
    else
        return 0;
}

int main_test(void)
{
    int m,n,k= 0;
    int j = 0, line = 0;
    static int nums[MAX_SIZE][MAX_SIZE] = {0};

    scanf("%d%d%d", &m, &n,&k);
    while (line < k)
    {
        readLine(nums[line], n);
        line++;
    }
    line = 0;
    while (line < k)
    {
        if (checkValidation(nums[line], m, n))
            printf("yes\n");
        else
            printf("no\n");
        line++;
    }
    return 0;
}