/*
 * @Author: your name
 * @Date: 2020-05-27 14:46:05
 * @LastEditTime: 2020-05-27 14:52:55
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \AlgorithmLearn\TreeTraversal.c
 */ 
/*  push 对应这树的前序遍历结果，pop对应着树的中序节点，根据中序和前序可以构建一棵树*/
#include <stdio.h>
#include <string.h>
#define MAX_N 30
typedef struct T_node *BinTree;
typedef struct T_node
{
    int value;
    BinTree left;
    BinTree right;
}Tree;
Tree T[MAX_N] = {0};
int spaceflag = 0;
void PostOrderTraversal(Tree *pT)
{
    if (pT == NULL)
        return;
    PostOrderTraversal(pT->left);
    PostOrderTraversal(pT->right);
    if (spaceflag == 1)
    {
        printf(" ");
    }
    else
    {
        spaceflag = 1;
    }
    printf("%d", pT->value);
}

int main(void)
{
    int num = 0, i = 0;
    int top = -1;
    int stack[MAX_N] = {0};
    int popValue = 0;
    
    scanf("%d", &num);
    while(i < (2 * num))
    {
        char signature[10] = {0}; 
        char idx = 0;
        
        scanf("%s", signature);
        
        if (strcmp(signature, "Push") == 0)
        {
            scanf(" %c", &idx);
            idx -= '0';
            if (popValue)
            {
                T[idx].value = idx;
                T[popValue].right = &T[idx];
            }
            else
            {
                T[idx].value = idx;
                if (idx > 1)
                {
                    T[idx-1].left = &T[idx];
                }
            }
            stack[++top] = idx;
            popValue = 0;
        }
        else
        {
            popValue = stack[top--];
        }
        i++;
    }
    PostOrderTraversal(&T[1]);
    return 0;
}