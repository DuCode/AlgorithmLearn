/*
 * @Author: your name
 * @Date: 2020-06-04 14:43:08
 * @LastEditTime: 2020-06-05 08:51:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \AlgorithmLearn\mutext.c
 */ 

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>

struct msg
{
    struct msg *next;
    int num;
};

struct msg *head;
pthread_cond_t has_product = PTHREAD_COND_INITIALIZER;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void *consumer(void *p)
{
    struct msg *mp;
    while(1)
    {
        pthread_mutex_lock(&lock);
        while (head == NULL)
        {
            /* 释放锁，并且挂起等待 */
            pthread_cond_wait(&has_product, &lock);
        }
        /* 删除链表节点 */
        mp = head;
        head = head->next;
        pthread_mutex_unlock(&lock);
        printf("Consume %d\n", mp->num);
        free(mp);
        sleep(rand()%5);/* 休眠1-5s */
    }
}

void *producer(void *p)
{
    struct msg *mp = NULL;
    while (1)
    {
        mp = (struct msg *)malloc(sizeof(struct msg));
        mp->num = rand()%1000+1;
        printf("produce %d\n", mp->num);
        pthread_mutex_lock(&lock);
        mp->next = head;
        head = mp;
        pthread_mutex_unlock(&lock);
        pthread_cond_signal(&has_product);
        sleep(rand()%3);
    }
    
}


int main(void)
{
    pthread_t pid, cid;
    srand(time(NULL));
    pthread_create(&pid, NULL, producer, NULL);
    pthread_create(&cid, NULL, consumer, NULL);
    pthread_join(pid, NULL);
    pthread_join(cid, NULL);
    return 0;
}