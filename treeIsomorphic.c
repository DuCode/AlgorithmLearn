/*
 * @Author: your name
 * @Date: 2020-05-08 13:26:31
 * @LastEditTime: 2020-05-08 16:01:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \AlgorithmLearn\tree.c
 */
#include <stdio.h>
#define MAXTREE 10
#define null -1
typedef struct T_Tree_node
{
    char value;
    int leftchild;
    int rightchild;
    int attchflag;
}Tnode;
typedef int Tree;

Tnode Tree1[MAXTREE] = {0};
Tnode Tree2[MAXTREE] = {0};


int readData(Tnode List[])
{
    int num = 0;
    scanf("%d", &num);
    for (int i = 0; i < num; i++)
    {
        /* 这里%c前面有空格，可以吃掉之前缓冲区剩余的换行符和空格，也可以用getchar函数获取stdin中剩余的字符 */
        scanf(" %c %c %c", &List[i].value, &List[i].leftchild, &List[i].rightchild);
        if (List[i].leftchild != '-')
        {
            List[i].leftchild -= '0';
            List[List[i].leftchild].attchflag = 1;
        }
        else
            List[i].leftchild = null;
        
        if(List[i].rightchild != '-')
        {
            List[i].rightchild -= '0';
            List[List[i].rightchild].attchflag = 1;
        }
        else
            List[i].rightchild = null;
        
    }
    return num;
}
Tree buildTree(Tnode nodeList[], int num)
{
    for (int i = 0; i < num; i++)
    {
        if (nodeList[i].attchflag == 0)
        {
            return i;
        }
    }
    return null;
}

int IsIsomorphic(Tree T1, Tree T2)
{
    if (T1 == null && T2 == null) /* both empty: 认为是同构的*/
    {
        return 1;
    }
    if ((T1!=null && T2==null) || (T1==null && T2!=null))
    {
        return 0;
    }
    if (Tree1[T1].value != Tree2[T2].value)
    {
        return 0;
    }
    /* 左子树都不为空，且左子树的根节点相等，则比较左子树是否同构，右子数是否同构 */
    if ((Tree1[T1].leftchild != null && Tree2[T2].leftchild != null)
    && (Tree1[Tree1[T1].leftchild].value == Tree2[Tree2[T2].leftchild].value))
    {
        int a = IsIsomorphic(Tree1[T1].leftchild, Tree2[T2].leftchild);
        int b = IsIsomorphic(Tree1[T1].rightchild, Tree2[T2].rightchild);
        return (a && b);
    }
    else
    {
        return (IsIsomorphic(Tree1[T1].leftchild, Tree2[T2].rightchild)
                && IsIsomorphic(Tree1[T1].rightchild, Tree2[T2].leftchild));
    }
    
    
}

int main(void)
{
    int node_num = readData(Tree1);
    Tree T1 = buildTree(Tree1, node_num);
    node_num = readData(Tree2);
    Tree T2 = buildTree(Tree2, node_num);
    if (IsIsomorphic(T1, T2))
    {
        printf("%s\n", "Yes");
    }
    else
    {
        printf("%s\n", "No");
    }
    
    return 0;
}