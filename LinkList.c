/* 
    实现单链表
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct T_Poly
{
    int coe;
    int ex;
}Poly_t;

typedef Poly_t ElementType;
typedef struct LinkNode * pnode;

typedef struct LinkNode
{
    ElementType data;
    pnode next;
}Node_t;

typedef struct Tag_List
{
    pnode Head; /**< 头结点 */
    pnode Rear;
    int   length;
}List_t;

/* 建立新链表 */
List_t* LinkList_CreatList(void)
{
    List_t *myList = (List_t*)malloc(sizeof(List_t));
    pnode new = (Node_t*)malloc(sizeof(Node_t));
    new->data.coe = 0;
    new->data.ex = 0;
    new->next = 0;
    myList->Head = new;
    myList->Rear = new;
    myList->length = 0;
    return myList;
}

int LinkList_IsEmpty(List_t *myList)
{
    int ret = 0;
    if (myList->Rear == myList->Head->next)
    {
        ret = 1;
    }
    return ret;
}

pnode LinkList_FindElement(List_t *l, ElementType x)
{
    /* pnode p = l->Head->next;
    while (p != NULL && p->data != x)
            p = p->next;
    return p; */
}

void LinkList_InsertElement(List_t *list, ElementType x, pnode p)
{
    pnode pre = NULL;
    for (pre = list->Head; pre->next != NULL && pre->next != p; pre = pre->next);
    if (pre != NULL && pre != list->Head)
    {
        /* 没找到不添加 */
        pnode newnode = (pnode)malloc(sizeof(Node_t));
        newnode->data = x;
        newnode->next = NULL;
        newnode->next = p;
        pre->next = newnode;
        list->length++;
    }
    
}

void LinkList_InsertBack(List_t *list, ElementType x)
{
    pnode newnode = (pnode)malloc(sizeof(Node_t));
    newnode->data = x;
    newnode->next = NULL;
    list->Rear->next = newnode;
    list->Rear = newnode;
}

void LinkList_Attach(List_t *list, pnode newNode)
{
    list->Rear->next = newNode;
    list->Rear = newNode;
}

void LinkList_Delete(List_t *list, pnode p)
{
    if (!LinkList_IsEmpty(list)&&p)
    {
        pnode s = list->Head;
        while (s->next!= p)
            s = s->next;
        if (s != list->Head)
        {
            s->next = p->next;
            free(p);
            list->length--;
        }
    }
    
}

List_t *Add(List_t *p1, List_t *p2)
{
    List_t *pRetL = LinkList_CreatList();
    pnode pn1 = p1->Head->next;
    pnode pn2 = p2->Head->next;
    while (pn1 && pn2)
    {
        pnode new = (pnode)malloc(sizeof(Node_t));
        /* 维护两个指针，比较这两个指针所指向的指数的大小，大的那个节点接到结果链表中，并移动指针到下一个节点 */
        if (pn1->data.ex > pn2->data.ex)
        {
            *new = *pn1;
            new->next = NULL;
            LinkList_Attach(pRetL, new);
            pn1 = pn1->next;
        }
        else if (pn1->data.ex == pn2->data.ex)
        {
            /* 指数相等，相加，如果和为0，则不添加到结果里 */
            int temp = pn1->data.coe + pn2->data.coe;
            if (temp != 0)
            {
                new->data.coe = temp;
                new->data.ex = pn1->data.ex;
                new->next = NULL;
                LinkList_Attach(pRetL, new);
            }
            pn1 = pn1->next;
            pn2 = pn2->next;
        }
        else
        {
            /* pn2指数大，将pn2加入结果 */
            *new = *pn2;
            new->next = NULL;
            LinkList_Attach(pRetL, new);
            pn2 = pn2->next;
        }
    }
    while (pn1)
    {
        LinkList_Attach(pRetL, pn1);
        pn1 = pn1->next;
    }
    while (pn2)
    {
        LinkList_Attach(pRetL, pn2);
        pn2 = pn2->next;
    }
    return pRetL;
}

void Link_Insert(List_t *L, pnode node)
{
    pnode p = L->Head;
    int flag = 1;
    while (p->next && node)
    {
        if (p->next->data.ex < node->data.ex)
        {
            node->next = p->next;
            p->next = node;
            flag = 0;
            break;
        }
        else if (p->next->data.ex == node->data.ex)
        {
            int sum = p->next->data.coe + node->data.coe;
            if (sum != 0)
            {
                p->next->data.coe = sum;
                free(node);
            }
            else
            {
                pnode temp = p->next;
                p->next = temp->next;
                if (L->Rear == temp)
                {
                    L->Rear = p;
                }
                free(temp);
                free(node);
            }
            flag = 0;
            break;

        }
        p = p->next;
    }
    if (p && p->next == NULL && flag)
    {
        LinkList_Attach(L, node);
    }
}
/* 其中一个多项式为空，则返回0 0 另外，不应该加入系数为0的项!!!!!! */
List_t * MultiplePoly(List_t *L1, List_t *L2)
{
    /* 采用两两重循环:追个插入，初始值为第二个多项式的首项乘于第一个多项式，然后接下来计算一个值后在插入计算出的初始结果链表 */
    pnode p1 = L1->Head->next;
    pnode p2 = L2->Head->next;
    List_t *pRet = LinkList_CreatList();
    while (p1 && p2)
    {
        if ((p1->data.coe * p2->data.coe) != 0) /* 不应该加入系数为0的项!!!!!! */ 
        {
            pnode newnode = (pnode)malloc(sizeof(Node_t));
            newnode->data.coe = p1->data.coe * p2->data.coe;
            newnode->data.ex = p1->data.ex + p2->data.ex;
            newnode->next = NULL;
            LinkList_Attach(pRet, newnode);
        }
        p1 = p1->next;
    }
    if (p2)
        p2 = p2->next;

    while (p2)
    {
        p1 = L1->Head->next;
        while (p1)
        {
            if (p1->data.coe * p2->data.coe != 0)
            {
                pnode newnode = (pnode)malloc(sizeof(Node_t));
                newnode->data.coe = p1->data.coe * p2->data.coe;
                newnode->data.ex = p1->data.ex + p2->data.ex;
                newnode->next = NULL;
                Link_Insert(pRet, newnode);
            }
            p1 = p1->next;
        }
        p2 = p2->next;
    }
    return pRet;
}


List_t * ReadData(void)
{
    int num = 0u;
    int i = 0u;
    List_t *pL = LinkList_CreatList();
    scanf("%d", &num);
    {
        while (i < num)
        {
            Poly_t p = {0};
            scanf("%d %d", &p.coe, &p.ex);
            LinkList_InsertBack(pL, p);
            i++;
        }
    }
    return pL;
}

void printList(List_t *L)
{
    int flat = 0;
    pnode pn = L->Head->next;
    if (!pn)
    {
        printf("%d %d", 0, 0);
    }
    else
    {
        while (pn)
        {
            if(flat)
                printf(" ");
            else
                flat = 1;
            printf("%d %d", pn->data.coe, pn->data.ex);
            pn = pn->next;
        }
    }
}



void Test(void)
{
    List_t *p1 = ReadData();
    List_t *p2 = ReadData();
    List_t *Ret1 = Add(p1, p2);
    List_t *Ret2 = MultiplePoly(p1, p2);
    printList(Ret2);
    printf("\n");
    printList(Ret1);
    
}
/* int main(void)
{
    while (1)
        Test();
    return 0;
} */